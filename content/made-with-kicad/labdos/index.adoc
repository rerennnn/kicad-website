+++
title = "LABDOS - semiconductor based ionising radiation spectrometer"
projectdeveloper = "Universal Scientific Technologies s.r.o. (UST)"
projecturl = "https://www.ust.cz/UST-dosimeters/#labdos01"
"made-with-kicad/categories" = [
    "Instrumentation"
]
+++

link:https://github.com/UniversalScientificTechnologies/LABDOS01[LABDOS01] is an open-source spectrometer-dosimeter based on a silicon PIN diode and is intended for scientific research and experimental purposes. A USB-C port or JST-GH connector secures power and communication. The device can be used statically (located in a specific place e.g. laboratory or base) or in mobile applications (such as cars or UAVs). The spectrometer is housed in a 3D printed box, which brings essential mechanical resistance and allows future development of user enclosures and new integrations.

The aim of LABDOS01 is to make an open-source, accessible, high-quality, reliable, and straightforward measuring device - a radiation energy spectrometer for the scientific community.
