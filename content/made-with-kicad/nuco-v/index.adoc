+++
title = "NUCO-V"
projectdeveloper = "Dmitry Filimonchuk"
projecturl = "https://github.com/dmitrystu/nuco-v"
"made-with-kicad/categories" = [
    "Development Board"
]
+++

link:https://github.com/dmitrystu/nuco-v[NUCO-V] is a NUCLEO-64(C) compatible development board for the STM32F7 and STM32H7 series with integrated link:https://github.com/blacksphere/blackmagic[Black Magic Probe]
