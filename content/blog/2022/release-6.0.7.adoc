+++
title = "KiCad 6.0.7 Release"
date = "2022-07-27"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the latest series 6 stable
release.  The 6.0.7 stable version contains critical bug fixes and
other minor improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 6.0.6 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/17[KiCad 6.0.7
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 6.0.7 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/6.0/[6.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- Update message panel from PCB point editor and schematic drawing tools. https://gitlab.com/kicad/code/kicad/-/issues/11961[#11961]
- https://gitlab.com/kicad/code/kicad/-/commit/7dc6abd5339d7ce54ae101737b6e4743c8411044[Add version 3.2 to search candidates for wxWidgets.]
- Catch some crashes on shutdown. https://gitlab.com/kicad/code/kicad/-/issues/10698[#10698]
- Fix OpenGL swap interval logic. https://gitlab.com/kicad/code/kicad/-/issues/11751[#11751]

=== Schematic Editor
- Fix broken units in symbol browser. https://gitlab.com/kicad/code/kicad/-/issues/11881[#11881]
- Fix project rescue bug. https://gitlab.com/kicad/code/kicad/-/issues/11897[#11897]
- Fix missing ERC error when pins are stacked. https://gitlab.com/kicad/code/kicad/-/issues/11926[#11926]
- Use same hot key to force H/V mode as board editor. https://gitlab.com/kicad/code/kicad/-/issues/10858[#10858]
- Ensure that pin text and symbol fields are always visible in plots. https://gitlab.com/kicad/code/kicad/-/issues/11969[#11969]
- Fix rounding error when importing Altium schematics. https://gitlab.com/kicad/code/kicad/-/issues/11742[#11742]
- https://gitlab.com/kicad/code/kicad/-/commit/d53c6f8abe7a043b028d90c9739963261bb384e2[Fix find dialog properties when wxWidgets has been updated to 3.1+.]
- https://gitlab.com/kicad/code/kicad/-/commit/865bb54591322223474f0f04a3954aa442ffa662[Flag ERC error on pins that are not stacked.]
- Fix crash when adding symbol from library editor. https://gitlab.com/kicad/code/kicad/-/issues/11891[#11891] and https://gitlab.com/kicad/code/kicad/-/issues/11772[#11772]
- Make annotation case insensitive. https://gitlab.com/kicad/code/kicad/-/issues/11862 [#11862]

=== Symbol Editor
- Refresh drawing when common preferences are changed. https://gitlab.com/kicad/code/kicad/-/issues/11921[#11921]
- Fix name escaping issue. https://gitlab.com/kicad/code/kicad/-/issues/11939[#11939]

=== PCB Editor
- Fix crash when filling zones. https://gitlab.com/kicad/code/kicad/-/issues/9888[#9888]
- https://gitlab.com/kicad/code/kicad/-/commit/45329ceafa4df18002c3484006f1a2118d78cc01[Prevent creation of new wxString on stack for each clearance check.]
- Fix pad properties rounding errors when units are mils. https://gitlab.com/kicad/code/kicad/-/issues/11878[#11878]
- https://gitlab.com/kicad/code/kicad/-/commit/1f4b288664b1ea0a9e17f987b310281122f09a05[Remove plotting unusable map file formats (GERBER and HGPL).]
- Correct Python call for board design settings. https://gitlab.com/kicad/code/kicad/-/issues/11924[#11924]
- https://gitlab.com/kicad/code/kicad/-/commit/29d58ae063462cce944fae4d1aaacbbdf124dcf1[Correct KiCad2Step user specified origin help message.]
- Add Python API parameter to update UUID when loading footprints. https://gitlab.com/kicad/code/kicad/-/issues/11870[#11870]
- https://gitlab.com/kicad/code/kicad/-/commit/ef2bb0b6216aaf2562e49e1fc0f6907f91ac7eb1[Remove documentation strings from plot drill file panel.]
- Do not print background color when printing in black and white. https://gitlab.com/kicad/code/kicad/-/issues/11625[#11625]
- Fix handling KiCad2Step "virtual" key word. https://gitlab.com/kicad/code/kicad/-/issues/10563[#10563]
- Fix bug in PNS router when handling segment width testing. https://gitlab.com/kicad/code/kicad/-/issues/11990[#11990]
- https://gitlab.com/kicad/code/kicad/-/commit/9c50a9fa4ad5ddda80f1c716b07dc042e56efcdd[Fix custom pad shape fill mode issue.]
-  Enforce group selection to when selecting any part of the group when  greedy (right to left) selecting. https://gitlab.com/kicad/code/kicad/-/issues/11902[#11902]

=== Plugin Content Manager
- https://gitlab.com/kicad/code/kicad/-/commit/52157acbfee52cebd1ac7969ce5238accdc56152[Fix KiCad version restriction logic.]

=== Windows
- Updated to wxWidgets 3.1.7.
- https://gitlab.com/kicad/code/kicad/-/commit/6d26e8e3e8defb3d5cee46957c4b7e1c7eabec00[Performance improvements when refilling zones and panning with selected items.]
